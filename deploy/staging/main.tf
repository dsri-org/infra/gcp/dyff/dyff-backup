# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

module "root" {
  source = "../.."

  google_cloud_service_account_file = var.google_cloud_service_account_file
  environment                       = "staging"
  project                           = "staging-dyff-659606"
  remote_state_user                 = var.remote_state_user
  remote_state_password             = var.remote_state_password

  bucket_mapping = [
    { source = "alignmentlabs-backup-32357af06a2cb698", sink = "dyff-backup-5e5y6rru" },
    { source = "alignmentlabs-datasets-a1d118148ee7550e", sink = "dyff-datasets-r3950jk1" },
    { source = "alignmentlabs-outputs-6f70a71477535211", sink = "dyff-outputs-pvlobje4" },
    { source = "alignmentlabs-reports-c1caea9ce5861c9e", sink = "dyff-reports-y0qxjjed" },
  ]
}
