# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/56082910/terraform/state/staging"
    lock_address   = "https://gitlab.com/api/v4/projects/56082910/terraform/state/staging/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/56082910/terraform/state/staging/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }
}
