# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

module "root" {
  source = "../.."

  google_cloud_service_account_file = var.google_cloud_service_account_file
  environment                       = "production"
  project                           = "production-dyff-862492"
  remote_state_user                 = var.remote_state_user
  remote_state_password             = var.remote_state_password

  bucket_mapping = [
    { source = "alignmentlabs-backup-32357af06a2cb698", sink = "dyff-backup-e67k5wgh" },
    { source = "alignmentlabs-datasets-a1d118148ee7550e", sink = "dyff-datasets-cdxb9vco" },
    { source = "alignmentlabs-outputs-6f70a71477535211", sink = "dyff-outputs-y6rod67x" },
    { source = "alignmentlabs-reports-c1caea9ce5861c9e", sink = "dyff-reports-fwyqvf6n" },
  ]
}
