# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

variable "google_cloud_service_account_file" {
  type    = string
  default = "credentials.json"
}

variable "remote_state_user" {
  type = string
}

variable "remote_state_password" {
  type      = string
  sensitive = true
}
