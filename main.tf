# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

resource "kubernetes_namespace" "dyff_backup" {
  metadata {
    name = "dyff-backup"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# Storage transfer job

resource "google_storage_bucket_iam_member" "source1" {
  for_each = local.bucket_mapping_map
  bucket   = each.value.source
  member   = "serviceAccount:${data.google_storage_transfer_project_service_account.default.email}"
  role     = "roles/storage.legacyBucketReader"
}

resource "google_storage_bucket_iam_member" "source2" {
  for_each = local.bucket_mapping_map
  bucket   = each.value.source
  member   = "serviceAccount:${data.google_storage_transfer_project_service_account.default.email}"
  role     = "roles/storage.objectViewer"
}

resource "google_storage_bucket_iam_member" "sink" {
  for_each = local.bucket_mapping_map
  bucket   = each.value.sink
  member   = "serviceAccount:${data.google_storage_transfer_project_service_account.default.email}"
  role     = "roles/storage.legacyBucketWriter"
}

resource "google_storage_transfer_job" "transfer_job" {
  depends_on  = [google_storage_bucket_iam_member.source1, google_storage_bucket_iam_member.source2, google_storage_bucket_iam_member.sink]
  for_each    = local.bucket_mapping_map
  description = "transfer from gcp bucket to gcp bucket"
  project     = var.project

  transfer_spec {
    transfer_options {
      delete_objects_from_source_after_transfer = false
    }

    gcs_data_source {
      bucket_name = each.value.source
    }

    gcs_data_sink {
      bucket_name = each.value.sink
    }
  }

  schedule {
    schedule_start_date {
      year  = 2024
      month = 4
      day   = 1
    }
    schedule_end_date {
      year  = 2024
      month = 4
      day   = 1
    }
  }
}

# Backup restore job

resource "kubernetes_config_map" "backup_restore_config_map" {
  metadata {
    name      = "dyff-backup-restore"
    namespace = kubernetes_namespace.dyff_backup.metadata[0].name
  }

  # Note: We backup from workflows.state => workflows.events
  data = {
    DYFF_BACKUP__STORAGE_PATH             = "${local.storage_urls.DYFF_RESOURCES__BACKUP__STORAGE__URL}/kafka/${local.kafka_topics.DYFF_KAFKA__TOPICS__WORKFLOWS_STATE}"
    DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS = local.kafka.bootstrap_servers
    DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS  = local.kafka_topics.DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS
    DYFF_STORAGE__BACKEND : "dyff.storage.backend.s3.storage.S3StorageBackend"
    DYFF_STORAGE__S3__ENDPOINT          = local.storage.hostname
    DYFF_STORAGE__S3__INTERNAL_ENDPOINT = local.storage.hostname
    DYFF_STORAGE__S3__ACCESS_KEY        = local.storage.access_id
  }
}

resource "kubernetes_secret" "backup_restore_secret" {
  metadata {
    name      = "dyff-backup-restore"
    namespace = kubernetes_namespace.dyff_backup.metadata[0].name
  }

  data = {
    DYFF_STORAGE__S3__SECRET_KEY = local.storage.secret
  }
}
