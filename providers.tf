# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

provider "google" {
  credentials = file(var.google_cloud_service_account_file)
  project     = var.project

  default_labels = local.default_tags
}

provider "kubernetes" {
  host  = "https://${data.google_container_cluster.dyff.endpoint}"
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.dyff.master_auth[0].cluster_ca_certificate,
  )
}

provider "helm" {
  kubernetes {
    host  = "https://${data.google_container_cluster.dyff.endpoint}"
    token = data.google_client_config.provider.access_token
    cluster_ca_certificate = base64decode(
      data.google_container_cluster.dyff.master_auth[0].cluster_ca_certificate,
    )
  }
}
