# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

variable "environment" {
  type = string
}

variable "google_cloud_service_account_file" {
  type = string
}

variable "project" {
  type = string
}

variable "remote_state_user" {
  type = string
}

variable "remote_state_password" {
  type = string
}

variable "bucket_mapping" {
  type = list(object({ source = string, sink = string }))
}
