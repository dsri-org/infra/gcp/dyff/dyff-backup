# SPDX-FileCopyrightText:
# SPDX-License-Identifier:

locals {

  cluster_name = "${var.environment}-dyff-cloud"
  deployment   = "dyff-backup"
  name         = "${var.environment}-${local.deployment}"
  region       = "us-central1"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  bucket_mapping_map = {
    for bucket in var.bucket_mapping : bucket.source => bucket
  }

  kafka = data.terraform_remote_state.kafka.outputs

  kafka_topics = {
    DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS = local.kafka.topics_map["dyff.workflows.events"].name
    DYFF_KAFKA__TOPICS__WORKFLOWS_STATE  = local.kafka.topics_map["dyff.workflows.state"].name
  }

  storage = data.terraform_remote_state.storage.outputs

  storage_urls = {
    DYFF_RESOURCES__BACKUP__STORAGE__URL   = local.storage.buckets["backup"].s3_url
    DYFF_RESOURCES__DATASETS__STORAGE__URL = local.storage.buckets["datasets"].s3_url
    DYFF_RESOURCES__MODULES__STORAGE__URL  = local.storage.buckets["modules"].s3_url
    DYFF_RESOURCES__OUTPUTS__STORAGE__URL  = local.storage.buckets["outputs"].s3_url
    DYFF_RESOURCES__REPORTS__STORAGE__URL  = local.storage.buckets["reports"].s3_url
    # DYFF_RESOURCES__AUDITPROCEDURES__STORAGE__URL   = local.storage.buckets["auditprocedures"].s3_url
    # DYFF_RESOURCES__AUDITREPORTS__STORAGE__URL      = local.storage.buckets["auditreports"].s3_url
    # DYFF_RESOURCES__DATASOURCES__STORAGE__URL       = local.storage.buckets["datasources"].s3_url
    # DYFF_RESOURCES__INFERENCESERVICES__STORAGE__URL = local.storage.buckets["inferenceservices"].s3_url
    # DYFF_RESOURCES__MODELS__STORAGE__URL            = local.storage.buckets["models"].s3_url
    # DYFF_RESOURCES__WEBASSETS__STORAGE__URL         = local.storage.buckets["webassets"].s3_url
  }
}
